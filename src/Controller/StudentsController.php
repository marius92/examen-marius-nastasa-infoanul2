<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Omines\DataTablesBundle\Adapter\Doctrine\ORMAdapter;
use Omines\DataTablesBundle\Adapter\ArrayAdapter;
use Omines\DataTablesBundle\Column\TextColumn;
use Omines\DataTablesBundle\Controller\DataTablesTrait;

class StudentsController extends AbstractController
{

    /**
     * @Route("/students", name="students")
     */
    public function index()
    {
        return $this->render('students/index.html.twig', [
            'controller_name' => 'StudentsController',
        ]);
    }
// /** 
//    * @Route("/student/ajax") 
// */ 
// public function ajaxAction(Request $request) {  
//     $students = $this->getDoctrine() 
//        ->getRepository('StudentsRepository:Students') 
//        ->findAll();  
       
//     if ($request->isXmlHttpRequest() || $request->query->get('showJson') == 1) {  
//        $jsonData = array();  
//        $idx = 0;  
//        foreach($students as $student) {  
//           $temp = array(
//              'firstName' => $student->getName(),
//              'lastName' => $student->getName(),    
//              'email' => $student->getAddress(),  
//           );   
//           $jsonData[$idx++] = $temp;  
//        } 
//        return new JsonResponse($jsonData); 
//     } else { 
//        return $this->render('students/index.html.twig'); 
//     } 
//  }

    /**
     * @Route("/students/json", name="students")
     */
public function JsonResponse()
    {
        $students = $this->getDoctrine()
        ->getRepository(Student::class)
        ->findAll();
        $rows = $this->get('rows');
        $response = $rows->serialize($students,'json');
        return new Response($response);
    }
}
