/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you require will output into a single css file (app.css in this case)
require("../css/app.scss");

// Need jQuery? Install it with "yarn add jquery", then uncomment to require it.
const $ = require("jquery");
require("bootstrap");

console.log("Hello Webpack Encore! Edit me in assets/js/app.js");
import Vue from "vue";
import axios from "axios";
var jscounter = new Vue({
    el: '#js-counter',
    delimiters: ['{*', '*}'],
    data: {
        students: 2,
        professors: 2,

    },
     methods: {
       
        sum: function () {
            return this.students + " " + this.professors;
        }
    },
  mounted: function() {
    axios
      .get("http://127.0.0.1:8000/json")
      .then(response => (this.json = response));
  }
});
